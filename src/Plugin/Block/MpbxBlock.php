<?php

namespace Drupal\mpbx\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a MapBox block.
 *
 * @Block(
 *   id = "mpbx_block",
 *   admin_label = @Translation("MapBox"),
 * )
 */
class MpbxBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('mpbx.settings');
    return [
      '#title' => $this->t('MapBox'),
      '#description' => $this->t('MapBox block'),
      '#theme' => 'mpbx',
      '#attached' => [
        'drupalSettings' => [
          'mpbx' => [
            'accessToken' => $config->get('access_token'),
            'style' => $config->get('style'),
            'latitude' => $config->get('coordinates.lat'),
            'longitude' => $config->get('coordinates.lon'),
            'zoom' => $config->get('zoom'),
            'marker' => $config->get('marker.url'),
            'anchor' => $config->get('anchor'),
            'interactive' => $config->get('interactive'),
          ],
        ],
      ],
      '#contextual_links' => [
        'mpbx' => [
          'route_parameters' => [],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
  }

}
