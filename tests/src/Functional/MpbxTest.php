<?php

namespace Drupal\Tests\mpbx\Functional;

use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the MPBX module.
 *
 * @group mpbx
 */
class MpbxTest extends BrowserTestBase {
  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static array $modules = [
    'block',
    'mpbx',
  ];

  /**
   * {@inheritdoc}
   */
  protected string $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->drupalLogin($this->rootUser);
  }

  /**
   * Testing the presence of a block on the page.
   */
  public function testBlock() {

    $this->drupalPlaceBlock('mpbx_block');

    $this->drupalGet('');

    $this->assertSession()
      ->pageTextContains('MapBox');

  }

  /**
   * Testing the configuration form.
   */
  public function testConfiguration() {

    $this->drupalGet('admin/structure/mpbx');

    $this->assertSession()
      ->statusCodeEquals(200);

    $this->getSession()
      ->getPage()
      ->pressButton('Save configuration');

    $this->assertSession()
      ->pageTextContains('The configuration options have been saved.');

  }

}
