(function ($, Drupal) {
    Drupal.behaviors.mapbox = {
        attach: function (context, settings) {
            $('#map').once('showMap').each(function () {
                var accessToken = settings.mpbx.accessToken;
                var style = settings.mpbx.style;
                var longitude = settings.mpbx.longitude;
                var latitude = settings.mpbx.latitude;
                var center = [longitude, latitude];
                var zoom = settings.mpbx.zoom;
                var markerUrl = settings.mpbx.marker;
                var interactive = settings.mpbx.interactive;

                var container = 'map';

                mapboxgl.accessToken = accessToken;

                var map = new mapboxgl.Map({
                    container: container,
                    style: style,
                    center: center,
                    zoom: zoom,
                    interactive: interactive,
                });

                if (markerUrl) {
                    var marker = document.createElement('div');
                    var markerInner = document.createElement('img');

                    marker.className = 'marker';
                    marker.style.width = '50px';
                    marker.style.height = '50px';

                    markerInner.src = markerUrl;

                    marker.appendChild(markerInner);

                    var options = {
                        anchor: settings.mpbx.anchor,
                    };

                    new mapboxgl.Marker(marker, options)
                        .setLngLat(center)
                        .addTo(map);
                }
            });
        },
    };
})(jQuery, Drupal)
